import React from 'react'
import {Form, Button} from 'react-bootstrap'

export default function Register(){
	return(

		<Form>
		  <Form.Group controlId="formBasicEmail">
		    <Form.Label>Email address</Form.Label>
		    <Form.Control type="email" placeholder="Enter email" />
		    <Form.Text className="text-muted">
		      We'll never share your email with anyone else.
		    </Form.Text>
		  </Form.Group>

		  <Form.Group controlId="formFullName">
		    <Form.Label>Full name:</Form.Label>
		    <Form.Control type="text" placeholder="Firstname Lastname ex. Pedro Penduko" />
		  </Form.Group>

		  <Form.Group controlId="formBasicPassword">
		    <Form.Label>Password</Form.Label>
		    <Form.Control type="password" placeholder="Password" />
		  </Form.Group>
		
		  <Button variant="primary" type="submit">
		    Sign up
		  </Button>
		</Form>

		)
}