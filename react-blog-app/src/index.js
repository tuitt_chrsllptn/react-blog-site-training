import React from 'react'; //imports react module as dependency of our app
import ReactDOM from 'react-dom'; //imports react dom module as dependency of our app, it is mainly used for rendering components to the DOM
import 'bootstrap/dist/css/bootstrap.min.css';
import App from './App'




ReactDOM.render(<App/> , document.getElementById('root'));
/*
	JSX (Javascript XML) - At allows us to write "HTML-like" code in React which makes it easier for us to write and add HTML tags in React JS
*/