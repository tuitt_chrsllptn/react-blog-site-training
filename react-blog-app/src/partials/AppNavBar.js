import React from 'react'
import {Navbar, Nav, NavDropdown, Form, Button, FormControl} from 'react-bootstrap'
const AppNavBar = () => {
	return(
		<Navbar bg="dark" expand="lg" variant="dark">
		  <Navbar.Brand href="#home">Zuitt Blogsite</Navbar.Brand>
		  <Navbar.Toggle aria-controls="basic-navbar-nav" />
		  <Navbar.Collapse id="basic-navbar-nav">
		    <Nav className="mr-auto">
		      <Nav.Link href="#home">Home</Nav.Link>
		      <Nav.Link href="#link">Blogs</Nav.Link>
		      <NavDropdown title="Dropdown" id="basic-nav-dropdown">
		        <NavDropdown.Item href="#action/3.1">Register</NavDropdown.Item>
		        <NavDropdown.Item href="#action/3.2">Login</NavDropdown.Item>
		        <NavDropdown.Item href="#action/3.3">Profile</NavDropdown.Item>
		        <NavDropdown.Divider />
		        <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
		      </NavDropdown>
		    </Nav>
		    <Form inline>
		      <FormControl type="text" placeholder="Search" className="mr-sm-2" />
		      <Button variant="outline-success">Search</Button>
		    </Form>
		  </Navbar.Collapse>
		</Navbar>
		)
}

export default AppNavBar