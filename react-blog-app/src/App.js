import React from 'react'
import {Container, Row, Col} from 'react-bootstrap'
import AppNavBar from './partials/AppNavBar'
import MainContent from './subcomponents/MainContent'
import Register from './pages/Register'

function App(){
	return(
		<>
		<AppNavBar/>
		<Container>
			<MainContent/>
			<Register/>
		</Container>
		</>
		)
}

export default App