import React from 'react'
import {Jumbotron, Button} from 'react-bootstrap'

export default function MainContent(){
	return(
		 <Jumbotron className="mt-2">
			<h1>Zuitt Blogsite App</h1>
			<p>
				Lorem ipsum dolor, sit amet consectetur adipisicing elit. Nobis, repudiandae at atque temporibus numquam vitae unde aliquam nihil similique veniam quae libero quis dignissimos tenetur corporis nulla sapiente eos, veritatis.

				Lorem ipsum dolor, sit amet consectetur adipisicing elit. Nobis, repudiandae at atque temporibus numquam vitae unde aliquam nihil similique veniam quae libero quis dignissimos tenetur corporis nulla sapiente eos, veritatis.

				Lorem ipsum dolor, sit amet consectetur adipisicing elit. Nobis, repudiandae at atque temporibus numquam vitae unde aliquam nihil similique veniam quae libero quis dignissimos tenetur corporis nulla sapiente eos, veritatis.
			</p>
			<p>
				Lorem ipsum dolor, sit amet consectetur adipisicing elit. Nobis, repudiandae at atque temporibus numquam vitae unde aliquam nihil similique veniam quae libero quis dignissimos tenetur corporis nulla sapiente eos, veritatis.

				Lorem ipsum dolor, sit amet consectetur adipisicing elit. Nobis, repudiandae at atque temporibus numquam vitae unde aliquam nihil similique veniam quae libero quis dignissimos tenetur corporis nulla sapiente eos, veritatis.

				Lorem ipsum dolor, sit amet consectetur adipisicing elit. Nobis, repudiandae at atque temporibus numquam vitae unde aliquam nihil similique veniam quae libero quis dignissimos tenetur corporis nulla sapiente eos, veritatis.
			</p>
			<p>
				<Button variant="primary">Learn more</Button>
			</p>
		</Jumbotron>
		)
}
