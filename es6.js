/*Variable declarations
	- let keyword instead of var and const
	- scoping
	- let keyword, allows us to declare variables that are limited to the scope of a block statement or expression on w/c it is used.
	- var keyword, it declares variable globally to an entire function regardless of block scope
	- hoisting - JS default behavior of moving declarations to the top. A JS variable can declared it has been used. 
	- using let keyword, the hoisting behavior will stop

*/

function sayHello(){
	let greetings = "Hello!";

	console.log(greetings);
}

sayHello();

// console.log(greetings)
// console.log(firstName);

// var firstName; //hoisting
// let firstName;

function createPerson(){
	let person = firstName;
}

/*Template Literals are string literals allowing us to embed expressions and operations. We can use multi-line strings and string interpolation features with them

Template literals are enclosed by the backtick(``), to embed expressions we use the dollar sign and curly braces (${expression})
*/

let fullName = "Pedro Penduko";
let company = "Zuitt PH";
let position = "IT Instructor";

console.log("Hi! I'm " + fullName + ",\ncurrently " + position + "\nat " + company)

let x = 5;
let y = 10;

console.log("Five plus Ten is equals to " + (x + y))


// Template literals
console.log(`Hi! I'm ${fullName},
currently ${position}
at ${company}
`)

console.log(`Five plus Ten is equals to ${x + y}`)

/*
	Arrow based functions
	1. Arrow based function syntax is much shorter and simpler in some cases
	2. Arrow function curly braces are NOT required IF only one expression is present
		ex. const add = (x, y) => x + y
	3. Arrow function dont need parenthesis IF there's only one argument.

		ex. const newNum = x => console.log(x)

	4. Arrow function has an implicit return
	5. Arrow functions don't have their one 'this' keyword
		-> JS 'this' keyword refers to the object where it belongs to
*/

// function createCar(color, brand, year, model) {
// 	console.log(`I have car, its model is ${model}, year ${year}, color ${color} and brand is ${brand}`)
// }

// const createCar = (color, brand, year, model) => {
//  	console.log(`I have car, its model is ${model}, year ${year}, color ${color} and brand is ${brand}`)
// }

// createCar("red", "Kia", 2019, "Soluto")

//
const car = color => console.log(`My car is color ${color}`)

car("red");


function getProduct(num1, num2) {
	return num1 * num2
}

const getSum = (num1, num2) => num1 + num2 

console.log(getProduct(2,5))
console.log(getSum(8,9))

/* JS 'this' keyword*/

let person = {
	firstName: "Pedro",
	lastName: "Penduko",
	studentID: "ZuittPH-123456",
	fullName: function(){
		return this.firstName + " " + this.lastName
	},
	studentDetails: () => {
		console.log(`${this.firstName} ${this.lastName} ${this.studentID}`)
	}
}

console.log(person.fullName())
person.studentDetails()

/*Destructuring Arrays and Objects*/

let names = ["Juan", "Pedro", "Paul"]
// console.log(names[0])
// console.log(names[1])
// console.log(names[2])
// let [John, Peter, Saul] =  ["Juan", "Pedro", "Paul"]
let [John, Peter, Saul] = names;
	console.log(John)
	console.log(Peter)
	console.log(Saul)

let mobilePhone = {
	brand: "Samsung",
	model: "Note 10",
	year: 2019,
	color: "Midnight Black"
}

let {brand, model, year, color} = mobilePhone

console.log(brand)
console.log(model)
console.log(year)
console.log(color)
// console.log(mobilePhone.model)
// console.log(mobilePhone.year)
// console.log(mobilePhone.color)